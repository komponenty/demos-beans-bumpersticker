package pk.demo.beans.bumpersticker;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author mikus
 */
public class BumperSticker extends JLabel {
    private static final String HEADER = "<html><big>I <font color=";
    private static final String INNER = ">&hearts;</font> ";
    private static final String FOOTER = "</big></html>";
    
    private int rate = 60;
    private Timer timer;
    private int state = 0;
    private String whatLove = "Component Oriented Programming";
    private int color = 0;

    public BumperSticker() {
        setHorizontalAlignment(CENTER);
        draw();
    }
    
    public String getWhatLove() {
        return whatLove;
    }
    
    public void setWhatLove(String whatLove) {
        this.whatLove = whatLove;
    }
    
    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void go() {
        if (timer != null) 
            return;
        timer = new Timer();

        long interval = 1935 / rate;
        timer.scheduleAtFixedRate(new BeatTask(), 0L, interval);
    }
    
    public void stop() {
        if (timer == null) 
            return;
        timer.cancel();
        timer = null;
    }
    
    private void draw() {
        String colorString = String.format("%06x", color);
        setText(HEADER + colorString + INNER + getWhatLove() + FOOTER);
    }

    public static void main(String[] args) {
        JFrame f = new JFrame("BumperSticker");
        f.setDefaultCloseOperation(3);
        f.setBounds(200, 200, 550, 100);
        Container container = f.getContentPane();
        container.setLayout(new BorderLayout());
        BumperSticker b = new BumperSticker();
        b.go();
        f.add(b, BorderLayout.CENTER);
        f.setVisible(true);
    }

    private class BeatTask extends TimerTask {
        
        @Override
        public void run() {
            int scale = state;
            
            if (state > 15) {
                scale = 31 - state;
            }

            color = (scale << 4 | scale) << 16;

            if (++state >= 31) {
                state = 0;
            }
            
            draw();
        }
    }
}
